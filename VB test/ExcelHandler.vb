﻿Public Class ExcelHandler

    Function CreateExcel(masterList As Dictionary(Of String, Int16), writeDict As Dictionary(Of String, String))
        Dim firstCall As Boolean
        firstCall = True

        For Each pair In writeDict
            If firstCall Then
                'If pair.Value Is Nothing Then
                'objSheet.Cells(objSheet.UsedRange.Rows.Count + 1, GetFromMasterList(masterList, pair.Key) + 1) = "NOT FOUND"
                'ElseIf pair.Value = "" Then
                'objSheet.Cells(objSheet.UsedRange.Rows.Count + 1, GetFromMasterList(masterList, pair.Key) + 1) = " "
                'Else
                'objSheet.Cells(objSheet.UsedRange.Rows.Count + 1, GetFromMasterList(masterList, pair.Key) + 1) = pair.Value
                'End If
                objSheet.Cells(objSheet.UsedRange.Rows.Count + 1, 1) = objSheet.UsedRange.Rows.Count
                firstCall = False
            Else
                If pair.Value Is Nothing Then
                    objSheet.Cells(objSheet.UsedRange.Rows.Count, GetFromMasterList(masterList, pair.Key) + 1) = "NOT FOUND"
                ElseIf pair.Value = "" Then
                    objSheet.Cells(objSheet.UsedRange.Rows.Count, GetFromMasterList(masterList, pair.Key) + 1) = " "
                Else
                    objSheet.Cells(objSheet.UsedRange.Rows.Count, GetFromMasterList(masterList, pair.Key) + 1) = pair.Value
                End If
            End If
        Next
        For Each pair In masterList
            objSheet.Cells(1, pair.Value + 1) = pair.Key
        Next
        objBook.SaveAs("C:\temp\SolidworksExtract.xlsx")
        Return masterList
    End Function
End Class
