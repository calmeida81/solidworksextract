﻿Public Class SolidworksInfo
    Function GetFileSummary(swModel As SldWorks.ModelDoc2, writeDict As Dictionary(Of String, String))
        writeDict.Add("DEFAULT-Title", swModel.SummaryInfo(0))
        writeDict.Add("DEFAULT-Subject", swModel.SummaryInfo(1))
        writeDict.Add("DEFAULT-Author", swModel.SummaryInfo(2))
        writeDict.Add("DEFAULT-Keywords", swModel.SummaryInfo(3))
        writeDict.Add("DEFAULT-Comment", swModel.SummaryInfo(4))
        writeDict.Add("DEFAULT-Saved By", swModel.SummaryInfo(5))
        writeDict.Add("DEFAULT-Date Created", swModel.SummaryInfo(6))
        writeDict.Add("DEFAULT-Date Saved", swModel.SummaryInfo(7))
        writeDict.Add("DEFAULT-Date Created 2", swModel.SummaryInfo(8))
        writeDict.Add("DEFAULT-Date Saved 2", swModel.SummaryInfo(9))
        Return writeDict
    End Function


    'http://help.solidworks.com/2018/english/api/sldworksapi/get_and_set_sheet_properties_example_vb.htm
    Function GetSheetInfo(swModel As SldWorks.ModelDoc2, masterList As Dictionary(Of String, Int16), writeDict As Dictionary(Of String, String))
        Dim swSheet As SldWorks.Sheet
        Dim swDraw As SldWorks.DrawingDoc
        Dim vSheetNames As Object
        Dim newSheet As SldWorks.Sheet
        Dim excelHandler As New ExcelHandler
        swSheet = swModel.GetCurrentSheet
        swDraw = swModel
        vSheetNames = swDraw.GetSheetNames
        Dim sheet As String
        For Each sheet In vSheetNames
            swDraw.ActivateSheet(sheet)
            newSheet = swModel.GetCurrentSheet
            Dim cloneDict As New Dictionary(Of String, String)
            For Each pair In writeDict
                cloneDict.Add(pair.Key, pair.Value)
            Next
            cloneDict.Add("Sheet Name", sheet)
            cloneDict.Add("Sheet", newSheet.GetTemplateName)
            masterList = ExcelHandler.CreateExcel(masterList, cloneDict)
        Next
        Return masterList
    End Function



    Function GetConfig(swModel As SldWorks.ModelDoc2, masterList As Dictionary(Of String, Int16), writeDict As Dictionary(Of String, String))
        Dim vConfigNameArr As Object
        Dim vConfigName As Object
        Dim vCustInfoNameArr As Object
        Dim vCustInfoName As Object
        Dim excelHandler As New ExcelHandler
        Try
            vConfigNameArr = swModel.GetConfigurationNames
        Catch ex As Exception

        End Try

        ' Is empty if a drawing because configurations not supported in drawings
        If IsNothing(vConfigNameArr) Then
            ReDim vConfigNameArr(0)
            vConfigNameArr(0) = ""
        Else
            'Add a blank string for the nonconfiguration-specific custom properties
            ReDim Preserve vConfigNameArr(UBound(vConfigNameArr) + 1)
        End If

        For Each vConfigName In vConfigNameArr
            Dim cloneDict As New Dictionary(Of String, String)

            For Each pair In writeDict
                cloneDict.Add(pair.Key, pair.Value)
            Next
            'For i = vConfigNameArr.Length - 1 To 0 Step -1 ' Use this if need to go from end
            'Console.WriteLine(vConfigName)
            cloneDict.Add("Config Name", vConfigName)
            vCustInfoNameArr = swModel.GetCustomInfoNames2(vConfigName)
            If Not IsNothing(vCustInfoNameArr) Then
                'For i = vCustInfoNameArr.Length To 1 Step -1
                'Console.WriteLine("  " & vCustInfoName & ", " & swModel.CustomInfo2(vConfigName, vCustInfoName))
                'Next
                For Each vCustInfoName In vCustInfoNameArr
                    'Console.WriteLine(vCustInfoName & ", " & swModel.CustomInfo2(vConfigName, vCustInfoName))
                    cloneDict.Add(vCustInfoName, swModel.CustomInfo2(vConfigName, vCustInfoName))
                Next
            End If
            masterList = excelHandler.CreateExcel(masterList, cloneDict)
            'Console.WriteLine("---------------------------")
        Next

        ' http://help.solidworks.com/2017/English/api/sldworksapi/Get_List_Of_Configurations_Example_VBNET.htm


        'For each part: All default attributes and all custom attributes
        'Find all references
        'For each drawing get above, PLUS get each sheet format filepath

        Return masterList
    End Function

    Function GetDrawingConfig(swModel As SldWorks.ModelDoc2, writeDict As Dictionary(Of String, String))
        Dim vConfigNameArr As Object
        Dim vConfigName As Object
        Dim vCustInfoNameArr As Object
        Dim vCustInfoName As Object
        Dim excelHandler As New ExcelHandler
        Try
            vConfigNameArr = swModel.GetConfigurationNames
        Catch ex As Exception

        End Try

        ' Is empty if a drawing because configurations not supported in drawings
        If IsNothing(vConfigNameArr) Then
            ReDim vConfigNameArr(0)
            vConfigNameArr(0) = ""
        Else
            'Add a blank string for the nonconfiguration-specific custom properties
            ReDim Preserve vConfigNameArr(UBound(vConfigNameArr) + 1)
        End If

        For Each vConfigName In vConfigNameArr
            vCustInfoNameArr = swModel.GetCustomInfoNames2(vConfigName)
            If Not IsNothing(vCustInfoNameArr) Then
                For Each vCustInfoName In vCustInfoNameArr
                    writeDict.Add(vCustInfoName, swModel.CustomInfo2(vConfigName, vCustInfoName))
                Next
            End If
        Next
        Return writeDict
    End Function

    Function GetReferences(documentName As String, swApp As SldWorks.SldWorks, writeDict As Dictionary(Of String, String))
        Dim value As Object
        Dim item As String
        value = swApp.GetDocumentDependencies2(documentName, True, True, False)
        Dim counter As Int32
        Dim allReferences As String = ""
        Dim first As Boolean = True
        counter = 0
        Try
            For Each item In value
                counter += 1
                If counter Mod 2 = 0 Then
                    If first Then
                        allReferences = item
                        first = False
                    Else
                        allReferences = allReferences & "," & item
                    End If
                End If
            Next
        Catch ex As NullReferenceException
            Return writeDict
        End Try

        writeDict.Add("References", allReferences)
        Return writeDict
    End Function


    'References: http://help.solidworks.com/2014/English/api/sldworksapi/solidworks.interop.sldworks~solidworks.interop.sldworks.isldworks~getdocumentdependencies2.html

    'http://help.solidworks.com/2016/english/api/sldworksapi/add_and_get_custom_properties_example_vb.htm
    Sub CustomProperties(swModel As SldWorks.ModelDoc2)
        Dim swSelMgr As SldWorks.SelectionMgr
        Dim swFeat As SldWorks.Feature
        Dim swCustPropMgr As SldWorks.CustomPropertyManager
        Dim vNameArr As Object
        Dim vName As Object

        swSelMgr = swModel.SelectionManager
        swFeat = swSelMgr.GetSelectedObject6(1, 0)
        swCustPropMgr = swFeat.CustomPropertyManager

        vNameArr = swCustPropMgr.GetNames : If IsNothing(vNameArr) Then Exit Sub
        For Each vName In vNameArr
            Console.WriteLine("    " & vName & " [" & swCustPropMgr.GetType(vName) & "] = " & swCustPropMgr.Get(vName))
        Next vName
    End Sub
End Class
