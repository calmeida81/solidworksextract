﻿Imports SldWorks
Imports SolidWorks.Interop.sldworks
Imports SolidWorks.Interop.swconst
Imports System
Imports Microsoft.VisualBasic
Imports Microsoft.Office.Interop
Imports System.IO
Imports solidworks_extract.ExcelHandler
Imports solidworks_extract.SolidworksInfo

Module Module1

    Public objApp As Excel.Application = New Excel.Application()
    Public objBooks As Excel.Workbooks
    Public objBook As Excel._Workbook
    Public objSheets As Excel.Sheets
    Public objSheet As Excel._Worksheet


    Public Sub main()
        Dim masterList As New Dictionary(Of String, Int16)
        Dim processed As New List(Of String)
        Dim recovery As Boolean = False
        If System.IO.File.Exists("masterlist.txt") Then
            Console.WriteLine("Recovery file exists, do you want to use the recovery file?" & System.Environment.NewLine & "Answering 'No' WILL DELETE RECOVERY FILES" & System.Environment.NewLine & " 1. Yes" & System.Environment.NewLine & " 2. No")
            Dim Input As String = Console.ReadLine()
            If Input.ToUpper Like "Yes".ToUpper Then
                masterList = ReadMasterListFromFile(masterList)
                processed = ReadProcessedListFromFile(processed)
                recovery = True
            ElseIf Input.ToUpper Like "No".ToUpper Then
                Try
                    My.Computer.FileSystem.DeleteFile("processed.txt")
                    My.Computer.FileSystem.DeleteFile("masterlist.txt")
                Catch ex As FileNotFoundException

                End Try

            Else
                Console.WriteLine("Invalid answer")
                Exit Sub
            End If
        Else
            Try
                My.Computer.FileSystem.DeleteFile("processed.txt")
            Catch ex As FileNotFoundException
            End Try
        End If

        If Not recovery Then
            masterList = PopulateMasterList(masterList)
        End If

        Dim fileError As Long
        Dim fileWarning As Long
        Dim solidworksInfo As New SolidworksInfo

        objApp = New Excel.Application()
        objBooks = objApp.Workbooks
        'objBook = objBooks.Open("C:\temp\SolidworksExtract.xlsx")
        objBook = objBooks.Add
        objSheets = objBook.Worksheets
        objSheet = objSheets(1)
        objApp.Visible = True
        objApp.DisplayAlerts = False

        'documentName = "C:\\Users\\Gabriel Radinsky\\Desktop\\parts\\89800 Fuss.SLDDRW"
        'http://help.solidworks.com/2016/english/api/swconst/SOLIDWORKS.Interop.swconst~SOLIDWORKS.Interop.swconst.swDocumentTypes_e.html
        'swApp.OpenDoc6(documentName, 3, 2, "", fileError, fileWarning) ' http://help.solidworks.com/2016/english/api/sldworksapi/get_file_summary_information_example_vb.htm
        'swModel = swApp.ActiveDoc
        'http://help.solidworks.com/2016/english/api/swconst/solidworks.interop.swconst~solidworks.interop.swconst.swsumminfofield_e.html

        Dim masterListSize As Int64 = masterList.Count

        Try
            Console.WriteLine("Please Enter a folder to read from")
            Dim inputFolder = Console.ReadLine()
            Dim txtFilesArray As String() = Directory.GetFiles(inputFolder, "*", SearchOption.AllDirectories) '"C:\\Users\\Gabriel Radinsky\\Desktop\\parts\\"
            For Each item In txtFilesArray
                Try
                    If recovery Then
                        If processed.Contains(item) Then
                            Continue For
                        End If
                    End If


                    Dim swApp As SldWorks.SldWorks
                    swApp = New SldWorks.SldWorks()
                    Dim swModel As SldWorks.ModelDoc2
                    swApp.Visible = True
                    swApp.SetUserPreferenceToggle(True, 999999)
                    Dim writeDict As New Dictionary(Of String, String)

                    If Path.GetFileName(item).ToUpper() Like "_SLDPRT" Then
                        Dim tmp As String = item.Substring(0, item.Length - 7) & "test1.sldprt"
                        My.Computer.FileSystem.RenameFile(item, "test1.sldprt")
                        swApp.OpenDoc6(tmp, 1, 2, "", fileError, fileWarning)
                        swModel = swApp.ActiveDoc
                        writeDict = solidworksInfo.GetFileSummary(swModel, writeDict)
                        writeDict.Add("DEFAULT-File Path", item)
                        writeDict = solidworksInfo.GetReferences(tmp, swApp, writeDict)
                        masterList = solidworksInfo.GetConfig(swModel, masterList, writeDict)
                        swApp.ExitApp()
                        swApp = Nothing
                        Threading.Thread.Sleep(2000)
                        My.Computer.FileSystem.RenameFile(tmp, "_SLDPRT")
                    ElseIf item.Substring(item.Length - 3).ToUpper() Like "PRT" And Path.GetFileName(item).ToUpper() IsNot "_SLDPRT" Then
                        swApp.OpenDoc6(item, 1, 2, "", fileError, fileWarning)
                        swModel = swApp.ActiveDoc
                        writeDict = solidworksInfo.GetFileSummary(swModel, writeDict)
                        writeDict.Add("DEFAULT-File Path", item)
                        writeDict = solidworksInfo.GetReferences(item, swApp, writeDict)
                        masterList = solidworksInfo.GetConfig(swModel, masterList, writeDict)
                        swApp.ExitApp()
                        swApp = Nothing

                    ElseIf Path.GetFileName(item).ToUpper() Like "_SLDASM" Then
                        Dim tmp As String = item.Substring(0, item.Length - 7) & "test1.sldasm"
                        My.Computer.FileSystem.RenameFile(item, "test1.sldasm")
                        swApp.OpenDoc6(tmp, 2, 2, "", fileError, fileWarning)
                        swModel = swApp.ActiveDoc
                        writeDict = solidworksInfo.GetFileSummary(swModel, writeDict)
                        writeDict.Add("DEFAULT-File Path", item)
                        writeDict = solidworksInfo.GetReferences(tmp, swApp, writeDict)
                        masterList = solidworksInfo.GetConfig(swModel, masterList, writeDict)
                        swApp.ExitApp()
                        swApp = Nothing
                        Threading.Thread.Sleep(2000)
                        My.Computer.FileSystem.RenameFile(tmp, "_SLDASM")
                    ElseIf item.Substring(item.Length - 3).ToUpper() Like "ASM" And Path.GetFileName(item).ToUpper() IsNot "_SLDASM" Then
                        swApp.OpenDoc6(item, 2, 2, "", fileError, fileWarning)
                        swModel = swApp.ActiveDoc
                        writeDict = solidworksInfo.GetFileSummary(swModel, writeDict)
                        writeDict.Add("DEFAULT-File Path", item)
                        writeDict = solidworksInfo.GetReferences(item, swApp, writeDict)
                        masterList = solidworksInfo.GetConfig(swModel, masterList, writeDict)
                        swApp.ExitApp()
                        swApp = Nothing

                    ElseIf Path.GetFileName(item).ToUpper() Like "_SLDDRW" Then
                        Dim tmp As String = item.Substring(0, item.Length - 7) & "test1.slddrw"
                        My.Computer.FileSystem.RenameFile(item, "test1.slddrw")
                        swApp.OpenDoc6(tmp, 3, 2, "", fileError, fileWarning)
                        swModel = swApp.ActiveDoc
                        writeDict = solidworksInfo.GetFileSummary(swModel, writeDict)
                        writeDict.Add("DEFAULT-File Path", item)
                        writeDict = solidworksInfo.GetReferences(tmp, swApp, writeDict)
                        writeDict = solidworksInfo.GetDrawingConfig(swModel, writeDict)
                        masterList = solidworksInfo.GetSheetInfo(swModel, masterList, writeDict)
                        swApp.ExitApp()
                        swApp = Nothing
                        Threading.Thread.Sleep(2000)
                        My.Computer.FileSystem.RenameFile(tmp, "_SLDDRW")
                    ElseIf item.Substring(item.Length - 3).ToUpper() Like "DRW" And Path.GetFileName(item).ToUpper() IsNot "_SLDDRW" Then
                        swApp.OpenDoc6(item, 3, 2, "", fileError, fileWarning)
                        swModel = swApp.ActiveDoc
                        writeDict = solidworksInfo.GetFileSummary(swModel, writeDict)
                        writeDict.Add("DEFAULT-File Path", item)
                        writeDict = solidworksInfo.GetReferences(item, swApp, writeDict)
                        writeDict = solidworksInfo.GetDrawingConfig(swModel, writeDict)
                        masterList = solidworksInfo.GetSheetInfo(swModel, masterList, writeDict)
                        swApp.ExitApp()
                        swApp = Nothing
                    End If
                    WriteFilesProcessed(item)
                Catch e As Exception
                    Console.WriteLine(e)
                    If masterList.Count > masterListSize Then
                        My.Computer.FileSystem.DeleteFile("masterlist.txt")
                        Threading.Thread.Sleep(1000)
                        WriteMasterListToFile(masterList)
                        masterListSize = masterList.Count
                    End If
                    Try
                        'swApp.ExitApp()
                    Catch ex As Exception
                        'swApp = Nothing
                        Continue For
                    End Try
                    'swApp = Nothing
                    Continue For
                End Try

            Next
            'objApp.UserControl = True

            objBook.SaveAs("C:\temp\SolidworksExtract.xlsx")
        Catch ex As Exception
            WriteMasterListToFile(masterList)
            objApp.Visible = True
            Console.WriteLine(ex)
        End Try
        Threading.Thread.Sleep(15000)
        objApp.Visible = True
        objSheet = Nothing
        objSheets = Nothing
        objBooks = Nothing
    End Sub


    Function ReadMasterListFromFile(masterList As Dictionary(Of String, Int16))
        Dim reader As StreamReader = New System.IO.StreamReader(File.OpenRead("masterlist.txt"))

        While reader.Peek() >= 0
            Dim line As String = reader.ReadLine()
            Dim values As String() = line.Split(",")
            Dim str As String = values(0)
            Dim int As Int16 = values(1)
            Console.WriteLine(str & " " & int)
            masterList.Add(str, int)
        End While
        reader.Close()
        Return masterList
    End Function

    Function ReadProcessedListFromFile(processed As List(Of String))
        Dim reader As StreamReader = New System.IO.StreamReader(File.OpenRead("processed.txt"))

        While reader.Peek() >= 0
            Dim line As String = reader.ReadLine()
            processed.Add(line)
        End While
        reader.Close()
        Return processed
    End Function

    Sub WriteMasterListToFile(masterList As Dictionary(Of String, Int16))
        Dim File = My.Computer.FileSystem.OpenTextFileWriter("masterlist.txt", True)
        For Each pair In masterList
            File.WriteLine(pair.Key & "," & pair.Value)
        Next
        File.Close()
    End Sub
    Sub WriteFilesProcessed(filePath As String)
        Dim File = My.Computer.FileSystem.OpenTextFileWriter("processed.txt", True)
        File.WriteLine(filePath)
        File.Close()
    End Sub


    Function PopulateMasterList(masterList As Dictionary(Of String, Int16))
        masterList.Add("ID", 0)
        masterList.Add("DEFAULT-Title", 1)
        masterList.Add("DEFAULT-Subject", 2)
        masterList.Add("DEFAULT-Author", 3)
        masterList.Add("DEFAULT-Keywords", 4)
        masterList.Add("DEFAULT-Comment", 5)
        masterList.Add("DEFAULT-Saved By", 6)
        masterList.Add("DEFAULT-Date Created", 7)
        masterList.Add("DEFAULT-Date Saved", 8)
        masterList.Add("DEFAULT-Date Created 2", 9)
        masterList.Add("DEFAULT-Date Saved 2", 10)
        masterList.Add("DEFAULT-File Name", 11)
        Return masterList
    End Function

    Function InsertIntoMasterList(masterList As Dictionary(Of String, Int16), newColumn As String)
        masterList.Add(newColumn, masterList.Count)
        Return masterList
    End Function

    Function GetFromMasterList(masterList As Dictionary(Of String, Int16), getColumn As String)
        Try
            Return masterList.Item(getColumn)
        Catch ex As KeyNotFoundException
            Return GetFromMasterList(InsertIntoMasterList(masterList, getColumn), getColumn)
        End Try
    End Function

End Module
